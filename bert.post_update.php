<?php

/**
 * @file
 * Post update functions for the BERT module.
 */

/**
 * Remove unnecessary 'entity_autocomplete' rows from the key_value table.
 */
function bert_post_update_8001(): string {
  $affectedRows = \Drupal::database()->delete('key_value')
    ->condition('collection', 'entity_autocomplete')
    ->condition('value', '%s:6:"entity";O:%', 'LIKE')
    ->execute();

  return sprintf('Deleted %d rows from the key_value table.', $affectedRows);
}
