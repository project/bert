<?php

namespace Drupal\bert\Plugin\bert\EntityReferenceLabelFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\bert\EntityReferenceLabelFormatterPluginBase;

/**
 * A label formatter displaying the entity title.
 *
 * @EntityReferenceLabelFormatter(
 *   id = "title",
 *   label = @Translation("Entity title"),
 * )
 */
class Title extends EntityReferenceLabelFormatterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getLabel(EntityInterface $entity): string {
    $entity = $this->entityRepository->getTranslationFromContext($entity);

    return $entity->label();
  }

}
