<?php

namespace Drupal\bert\Plugin\bert\EntityReferenceLabelFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\bert\EntityReferenceLabelFormatterPluginBase;

/**
 * A label formatter displaying the entity title and bundle.
 *
 * @EntityReferenceLabelFormatter(
 *   id = "title_bundle",
 *   label = @Translation("Entity title and bundle"),
 * )
 */
class TitleBundle extends EntityReferenceLabelFormatterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getLabel(EntityInterface $entity): string {
    $entity = $this->entityRepository->getTranslationFromContext($entity);
    $entityType = $entity->getEntityType();
    $bundle = $entity->get($entityType->getKey('bundle'))->entity;

    return sprintf(
      '%s (%s)',
      $entity->label(),
      $bundle->label()
    );
  }

}
