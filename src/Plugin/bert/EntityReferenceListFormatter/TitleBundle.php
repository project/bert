<?php

namespace Drupal\bert\Plugin\bert\EntityReferenceListFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bert\EntityReferenceListFormatterPluginBase;

/**
 * Displays the entity label and bundle.
 *
 * @EntityReferenceListFormatter(
 *   id = "title_bundle",
 *   label = @Translation("Entity title and bundle"),
 * )
 */
class TitleBundle extends EntityReferenceListFormatterPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getCells(EntityInterface $entity): array {
    $entity = $this->entityRepository->getTranslationFromContext($entity);
    $entityType = $entity->getEntityType();
    $bundle = $entity->get($entityType->getKey('bundle'))->entity;

    return [
      ['#markup' => $entity->label()],
      ['#markup' => $bundle->label()],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader(): array {
    return [
      $this->t('Title'),
      $this->t('Type'),
    ];
  }

}
