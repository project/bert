<?php

namespace Drupal\bert\Plugin\bert\EntityReferenceListFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Render\Markup;
use Drupal\bert\EntityReferenceListFormatterPluginBase;

/**
 * Displays the entity label linking to the edit form.
 *
 * @EntityReferenceListFormatter(
 *   id = "title_with_edit_link",
 *   label = @Translation("Entity title (with edit link)"),
 * )
 */
class TitleWithEditLink extends EntityReferenceListFormatterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getCells(EntityInterface $entity): array {
    $entity = $this->entityRepository->getTranslationFromContext($entity);

    try {
      return [[
        '#type' => 'link',
        '#title' => Markup::create($entity->label()),
        '#url' => $entity->toUrl('edit-form'),
        '#attributes' => [
          'target' => '_blank',
          'rel' => 'noreferrer noopener',
        ],
      ],
      ];
    }
    catch (UndefinedLinkTemplateException $e) {
      return [
        ['#markup' => Markup::create($entity->label())],
      ];
    }
  }

}
