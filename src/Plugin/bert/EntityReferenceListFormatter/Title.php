<?php

namespace Drupal\bert\Plugin\bert\EntityReferenceListFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bert\EntityReferenceListFormatterPluginBase;

/**
 * Displays the entity label.
 *
 * @EntityReferenceListFormatter(
 *   id = "title",
 *   label = @Translation("Entity title"),
 * )
 */
class Title extends EntityReferenceListFormatterPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getCells(EntityInterface $entity): array {
    $entity = $this->entityRepository->getTranslationFromContext($entity);

    return [
      ['#markup' => $entity->label()],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader(): array {
    return [
      $this->t('Title'),
    ];
  }

}
