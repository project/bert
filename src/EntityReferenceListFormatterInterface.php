<?php

namespace Drupal\bert;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the entity reference list formatter interface.
 */
interface EntityReferenceListFormatterInterface extends PluginInspectionInterface {

  /**
   * Return a render array with table cells.
   */
  public function getCells(EntityInterface $entity): array;

  /**
   * Return a render array with table header.
   */
  public function getHeader(): array;

}
