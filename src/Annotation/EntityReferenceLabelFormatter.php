<?php

namespace Drupal\bert\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EntityReferenceLabelFormatter annotation object.
 *
 * @Annotation
 */
class EntityReferenceLabelFormatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the formatter type.
   *
   * @var string
   * */
  protected $label;

}
