<?php

namespace Drupal\bert\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EntityReferenceListFormatter annotation object.
 *
 * @Annotation
 */
class EntityReferenceListFormatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the formatter type.
   *
   * @var string
   * */
  protected $label;

}
