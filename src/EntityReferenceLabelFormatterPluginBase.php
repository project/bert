<?php

namespace Drupal\bert;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a basic entity reference label formatter plugin.
 */
abstract class EntityReferenceLabelFormatterPluginBase extends PluginBase implements EntityReferenceLabelFormatterInterface, ContainerFactoryPluginInterface {

  /**
   * The entity being used by this plugin.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $parentEntity;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    $instance = new static($configuration, $pluginId, $pluginDefinition);
    $instance->entityRepository = $container->get('entity.repository');

    return $instance;
  }

  /**
   * Get the parent entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Return a content entity object or null.
   */
  public function getParentEntity() {
    return $this->parentEntity;
  }

  /**
   * Set the parent entity.
   *
   * @return $this
   */
  public function setParentEntity(ContentEntityInterface $parentEntity) {
    $this->parentEntity = $parentEntity;
    return $this;
  }

}
