<?php

namespace Drupal\bert;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the entity reference label formatter interface.
 */
interface EntityReferenceLabelFormatterInterface extends PluginInspectionInterface {

  /**
   * Returns a label that to represent the entity in search results.
   */
  public function getLabel(EntityInterface $entity): string;

}
