<?php

namespace Drupal\bert;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\bert\Annotation\EntityReferenceListFormatter;

/**
 * Provides the entity Reference list formatter manager.
 */
class EntityReferenceListFormatterManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/bert/EntityReferenceListFormatter',
      $namespaces,
      $module_handler,
      EntityReferenceListFormatterInterface::class,
      EntityReferenceListFormatter::class
    );
    $this->alterInfo('bert_entity_reference_list_formatter');
    $this->setCacheBackend($cache_backend, 'bert_entity_reference_list_formatter');
  }

}
