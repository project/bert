<?php

namespace Drupal\bert;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\bert\Annotation\EntityReferenceLabelFormatter;

/**
 * Provides the entity reference label formatter manager.
 */
class EntityReferenceLabelFormatterManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct(
      'Plugin/bert/EntityReferenceLabelFormatter',
      $namespaces,
      $moduleHandler,
      EntityReferenceLabelFormatterInterface::class,
      EntityReferenceLabelFormatter::class
    );
    $this->alterInfo('bert_entity_reference_label_formatter');
    $this->setCacheBackend($cacheBackend, 'bert_entity_reference_label_formatter');
  }

}
