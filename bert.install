<?php

/**
 * @file
 * Install, update and uninstall functions for bert module.
 */

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;

/**
 * Migrate from the wmbert module if it's installed.
 */
function bert_install(): void {
  if (!\Drupal::moduleHandler()->moduleExists('wmbert')) {
    return;
  }

  foreach (EntityFormDisplay::loadMultiple() as $formDisplay) {
    $changed = FALSE;

    foreach ($formDisplay->getComponents() as $fieldName => $component) {
      if (!isset($component['type']) || $component['type'] !== 'wmbert') {
        continue;
      }

      $component['type'] = 'bert';

      $formDisplay->setComponent($fieldName, $component);
      $changed = TRUE;
    }

    if ($changed) {
      $formDisplay->save();
    }
  }

  foreach (FieldConfig::loadMultiple() as $fieldConfig) {
    if ($fieldConfig->getType() !== 'entity_reference') {
      continue;
    }

    $handler = $fieldConfig->getSetting('handler');
    if (strpos($handler, 'wmbert') === FALSE) {
      continue;
    }

    $fieldConfig->setSetting('handler', str_replace('wmbert', 'bert', $handler));
    $fieldConfig->save();
  }

  \Drupal::getContainer()->get('module_installer')->uninstall(['wmbert']);
}
